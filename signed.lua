-- https://github.com/creationix/luajit-blake2
-- Shove ^that in your `lualib`
b = require('blake2b')

function fourohone(message)
    ngx.status = ngx.HTTP_UNAUTHORIZED
    ngx.header.content_type = "application/json; charset=utf-8"
    ngx.say("{\"error\": \"" .. message .. "\"}")
    ngx.exit(ngx.HTTP_UNAUTHORIZED)
end

-- We're expecting our token as `Authorization: Bearer XYZABC`
local auth_header = ngx.var.http_Authorization
if auth_header then
    _, _, token = string.find(auth_header, "Bearer%s+(.+)")
end

-- If we don't have a header or the token is empty, 401.
if (auth_header == nil or token == nil) then
    fourohone("missing header")
end

-- If we can't Base64 decode the token, 401.
d = ngx.decode_base64(token)
if d == nil then
    fourohone("undecodeable token")
end

-- If we don't have 24 bytes, 401.
if string.len(d) ~= 24 then
    fourohone("invalid token")
end

-- Our bMAC key from the `nginx` config.
key = ngx.var.bmac_key

-- If we don't have a set key, it's probably safest to 401
-- every request. But for testing, we'll use a default.
if key == nil then
    -- fourohone("no key set")
    key = "fishmonger"
end

body = string.sub(d, 1, 16)
hash = string.sub(d, 17, 24)

-- bmac our body to see if it matches the presented checksum.
q = b.hash(body, 8, key, 'string')
if q ~= hash then
    fourohone("mismatched checksum")
end

-- we're using `Expiry:32,MemberID:32,Claims:32,Spare:32` tokens here
-- but we're only using the expiry information for access control.
-- Alas, `luajit` doesn't have `string.unpack`; we have to manually frob.
expiry = 16777216*string.byte(body, 4) + 65536*string.byte(body, 3) + 256*string.byte(body, 2) + string.byte(body, 1)

now = os.time()
if expiry < now then
    fourohone("token expired")
end

-- If we make it here, we've got access. Set some helpful
-- headers for our downstream to interpret. Again, manual
-- frobbing because we don't have access to `unpack`.
member = 16777216*string.byte(body, 8) + 65536*string.byte(body, 7) + 256*string.byte(body, 6) + string.byte(body, 5)
ngx.req.set_header("X-Member", member)
claims = 16777216*string.byte(body, 12) + 65536*string.byte(body, 11) + 256*string.byte(body, 10) + string.byte(body, 9)
ngx.req.set_header("X-Claims", claims)
