# Signed Session Cookies

Supporting code for [Improving A Session Cookies](https://rjp.is/blogging/posts/session-cookies/) and [Further Session Cookies](https://rjp.is/blogging/posts/2019/05/further-session-cookies)

## Creating a token

    > go run bb.go enc tokenkey
    EX: 1558761098
    MI: 1298498081
    CC: 0x78629A0F
    is7oXCGCZU0PmmJ4AAAAAEWMKDgJYnUd

Our expiry time is 1558761098 (basically 'now + 1h'); memberID is 1298498081; and our claims are random.
The resulting token is `is7oXCGCZU0PmmJ4AAAAAEWMKDgJYnUd`.

## Testing a token

    > go run bb.go dec tokenkey is7oXCGCZU0PmmJ4AAAAAEWMKDgJYnUd
    EX: 1558761098
    MI: 1298498081
    CC: 0x78629A0F

Everything matches; we've successfully decoded the token and verified the signing.

# Using these tokens for access control

If you have [OpenResty](https://openresty.org/en/), [signed.lua](signed.lua) can be used as `access_by_lua_file`.

    location /testing/ {
        set $bmac_key 'tokenkey';
        access_by_lua_file /opt/openresty/luajit/signed.lua;
        root /data/rjp/www;
        index index.html;
    }

Passing the token is standard `Authorization: Bearer $TOKEN` format.

    curl -H "Authorization: Bearer is7oXCGCZU0PmmJ4AAAAAEWMKDgJYnUd" localhost:8280/testing/

If the token is incorrectly signed or has expired, you'll get a `401 Authorization Required` response.
