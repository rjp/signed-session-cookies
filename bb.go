package main

import (
	"bytes"
	"encoding/base64"
	"encoding/binary"
	"errors"
	"fmt"
	"golang.org/x/crypto/blake2b"
	"math/rand"
	"os"
	"time"
)

// Apparently Blake2 has an inbuilt keyed MAC mode
// which obviates the need for a double-tap HMAC.
func bmac(key string, message []byte) [8]byte {
	var checksum [8]byte
	keyBytes := []byte(key)

	b, err := blake2b.New(8, keyBytes)
	if err != nil {
		panic(err)
	}

	b.Write(message)
	copy(checksum[:], b.Sum(nil))

	return checksum
}

// We have 128 bits available; for our example, we're
// doing a simple token with three `uint32`s of information.
type TokenVal struct {
	expiry   int32
	memberID int32
	claims   int32
	padding  int32
}

func NewTokenRandom() *TokenVal {
	// Expires an hour from minting
	expiry := int32(time.Now().Add(3600 * time.Second).Unix())

	// A random Member ID that fits into 32 bits
	var memberID int32 = rand.Int31()

	// Random claims that fit into 32 bits
	var claims int32 = rand.Int31()

	// And our padding is empty but could be random
	padding := int32(0)

	return &TokenVal{expiry, memberID, claims, padding}
}

func (v *TokenVal) Print() {
	fmt.Printf("EX: %d\n", v.expiry)
	fmt.Printf("MI: %d\n", v.memberID)
	fmt.Printf("CC: 0x%08X\n", v.claims)
}

func enc(key string, token *TokenVal) string {
	// Generate a random token if we don't get one
	if token == nil {
		token = NewTokenRandom()
		// Obviously we need to know what our random values are
		token.Print()
	}

	// Pack our token into bytes
	buf := new(bytes.Buffer)
	_ = binary.Write(buf, binary.LittleEndian, token.expiry)
	_ = binary.Write(buf, binary.LittleEndian, token.memberID)
	_ = binary.Write(buf, binary.LittleEndian, token.claims)
	_ = binary.Write(buf, binary.LittleEndian, token.padding)

	// Calculate the Blake2 MAC
	checksum := bmac(key, buf.Bytes())

	// Append the checksum
	_, _ = buf.Write(checksum[:])

	// Since we're stuck with Base64 for space reasons,
	// we need to use the URL safe encoding.
	return base64.URLEncoding.EncodeToString(buf.Bytes())
}

func dec(token string, key string) (*TokenVal, error) {
	vals := &TokenVal{}

	data, err := base64.URLEncoding.DecodeString(token)
	if err != nil {
		return nil, err
	}

	// We need 24 bytes of data to process.
	if len(data) != 24 {
		return nil, errors.New("need 24 bytes")
	}

	buf := bytes.NewReader(data)

	// 4x`uint32` of data
	_ = binary.Read(buf, binary.LittleEndian, &vals.expiry)
	_ = binary.Read(buf, binary.LittleEndian, &vals.memberID)
	_ = binary.Read(buf, binary.LittleEndian, &vals.claims)
	_ = binary.Read(buf, binary.LittleEndian, &vals.padding)

	// And 8 bytes of checksum
	checksum := make([]byte, 8)
	_, _ = buf.Read(checksum)

	// What should the checksum be based on our data?
	check := bmac(key, data[0:16])

	// Not constant-time but we can live with this for now
	for i, _ := range check {
		if check[i] != checksum[i] {
			return nil, errors.New("mismatched checksum")
		}
	}

	return vals, nil
}

func main() {
	key := "testing1234"
	token := ""

	// Currently we only encode a random token.
	if os.Args[1] == "enc" {
		key = os.Args[2]
		cookie := enc(key, nil)
		fmt.Printf("%s\n", cookie)
	}

	// Decode a token and report the values.
	if os.Args[1] == "dec" {
		key = os.Args[2]
		token = os.Args[3]
		v, err := dec(token, key)
		if v != nil {
			v.Print()
		} else {
			fmt.Println(err)
		}
	}
}
